import random

def main():

    try:
        count = 0
        file = input('file name')
        num_counter = int(input('How many numbers do you want to write'))

        infile = open(file, 'w')

        for i in range(num_counter):
            num = random.randint(1, 500)
            count +=1
            infile.write(f'{count}: {num} \n')
    except:
        print('Something went wrong')



if __name__ == '__main__':
    main()
