def main():
    try:
        counter = 0
        sum = 0

        infile = input('Write a file name>>>')

        file = open(infile, 'r')

        for i in file:
            line = file.readline()
            line_int = int(line)
            counter += 1
            sum += line_int
            print(f'{counter}: {line_int}')
        print(f'--------\n'
              f'{counter}, {sum}')
    except ValueError as Er:
        print(f'{Er}')


if __name__ == '__main__':
    main()