def main():
    try:
        num = 0
        index = 0
        name = input('Enter the file`s name')
        infile = open(name, 'r')

        for i in infile:
            num = float(i)
            index += 1
            print(f'{index}: {num}')
    except:
        print('Something went wrong')


if __name__ == '__main__':
    main()