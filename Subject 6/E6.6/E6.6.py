def main():
    try:
        num_total = 0
        count = 0
        average = 0

        name = input('Enter the file`s name')
        infile = open(name, 'r')

        for i in infile:
            num = float(i)
            num_total += num
            count += 1
            average = num_total / count
        print(f'{num_total}, {count}, {average}')

    except:
        print('Something went wrong')


if __name__ == '__main__':
    main()