def main():
    name = input('Please, enter your name')
    description = input('Please, enter description here')

    try:
        infile = open('page.html', 'w')

        infile.write('<html> \n')
        infile.write('<head> \n')
        infile.write('</head>\n')
        infile.write('<body>\n')
        infile.write(' <center>\n')
        infile.write(f'<h1>{name}</h1>\n')
        infile.write(' </center>\n')
        infile.write(' <hr />\n')
        infile.write(f' {description}\n')
        infile.write(' <hr />\n')
        infile.write('</body>\n')
        infile.write('</html>')

        infile.close()
    except:
        print('Something went wrong')


if __name__ == '__main__':
    main()