JAN_DAYS = 31
FEB_DAYS = 28
MARCH_DAYS = 31
APRIL_DAYS = 30
MAY_DAYS = 31
JUNE_DAYS = 30
JULY_DAYS = 31
AUG_DAYS = 31
SEPT_DAYS = 30
OCT_DAYS = 31
NOV_DAYS = 30
DEC_DAYS = 31


def main():
    infile = open('steps.txt', 'r')

    average_steps(infile, 'январе', JAN_DAYS)
    average_steps(infile, 'феврале', FEB_DAYS)
    average_steps(infile, 'марте', MARCH_DAYS)
    average_steps(infile, 'апреле', APRIL_DAYS)
    average_steps(infile, 'мае', MAY_DAYS)
    average_steps(infile, 'июне', JUNE_DAYS)
    average_steps(infile, 'июле', JULY_DAYS)
    average_steps(infile, 'августе', AUG_DAYS)
    average_steps(infile, 'сентябре', SEPT_DAYS)
    average_steps(infile, 'октябре', OCT_DAYS)
    average_steps(infile, 'ноябре', NOV_DAYS)
    average_steps(infile, 'декабре', DEC_DAYS)

    infile.close()


def average_steps(steps, month_name, days):
    sum = 0
    for count in range(days):
        sum += int(steps.readline())
    average = sum / days
    print(f'В {month_name}\n'
          f'{average:.1f} шагов\n'
          f'{sum} сумма шагов')


if __name__ == '__main__':
    main()