def main():
    try:
        num_total = 0

        name = input('Enter the file`s name')
        infile = open(name, 'r')

        for i in infile:
            num = float(i)
            num_total += num
        print(f'{num_total}')

    except:
        print('Something went wrong')


if __name__ == '__main__':
    main()