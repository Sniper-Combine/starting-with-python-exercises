def main():
    players = int(input('How many players has been played'))

    file = open('golf.txt', 'w')

    intro_str = str('Player----Score\n')
    file.write(intro_str)
    for i in range(1, players + 1):
        name = input('Please, enter your name')
        score = input('Please, enter your score')
        file.write(name + '\n')
        file.write(score + '\n')

    file.close()


if __name__ == '__main__':
    main()

