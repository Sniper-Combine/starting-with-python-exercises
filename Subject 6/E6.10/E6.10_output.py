def main():
    file = open('golf.txt', 'r')

    line = file.readline()
    line = line.rstrip('\n')
    while line != '':
        print(line)
        line = file.readline()
        line = line.rstrip('\n')

    file.close()


if __name__ == '__main__':
    main()