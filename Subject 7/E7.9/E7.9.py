START_YEAR = 1950
END_YEAR = 1990


def main():
    list = []
    text = open('USPopulation.txt', 'r')
    list_str = text.readlines()
    for i in range(1, len(list_str)):
        l = float(list_str[i])
        list.append(l)

    average, max_year, min_year = counting(list)

    print(f'Average population growth is {average:.1f}\n'
          f'Maximum population growth seen in {max_year}\n'
          f'Minimum population growth seen in {min_year}')


def counting(list):
    sum = 0
    average = 0
    growth_list = []

    for i in range(1, len(list)):
        a = int(list[i-1])
        b = int(list[i])
        growth = b - a
        growth_list.append(growth)

    for i in range(0, len(growth_list)):
        sum += growth_list[i]
    average = sum / len(growth_list)            # !!!

    max_year = growth_list.index(max(growth_list)) + START_YEAR
    min_year = growth_list.index(min(growth_list)) + START_YEAR

    return average, max_year, min_year


if __name__ == '__main__':
    main()
