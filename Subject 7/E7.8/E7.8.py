

def main():

    print(f'This program will show you, \n'
          f'if an entered name is among of the most popular \n'
          f'names in 2000-2009\n'
          f'* Enter 1, if you want to find a boy name\n'
          f'* Enter 2, if you want to find a girl name\n'
          f'* Enter 3, if you want to find both girl and boy names\n'
          f'------------------------------------------------------')

    input_val = int(input('>>>'))

    if input_val != 1 and input_val != 2 and input_val != 3:
        print('Current value is incorrect')
    elif input_val == 1:
        boy_n = input('Please, enter a boy name here')
        boy_name(boy_n)
    elif input_val == 2:
        girl_n = input('Please, enter a girl name here')
        girl_name(girl_n)
    else:
        boy_n = input('Please, enter a boy name here')
        girl_n = input('Please, enter a girl name here')
        boy_name(boy_n)
        girl_name(girl_n)


def boy_name(val):
    boy_file = open('BoyNames.txt', 'r')
    boy_list = []
    check_name = False

    for i in boy_file:
        i = i.rstrip('\n')
        boy_list.append(i)

    for name in boy_list:
        if name == val:
            check_name = True

    if check_name:
        print(f'Name {val} is in the list')
    else:
        print(f'Name {val} isn`t in the list')


def girl_name(val):
    girl_file = open('GirlNames.txt', 'r')
    girl_list = []
    check_name = False

    for i in girl_file:
        i = i.rstrip('\n')
        girl_list.append(i)

    for name in girl_list:
        if name == val:
            check_name = True

    if check_name:
        print(f'Name {val} is in the list')
    else:
        print(f'Name {val} isn`t in the list')



if __name__ == '__main__':
    main()
