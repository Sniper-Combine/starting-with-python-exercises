NUM_VALUE = 20

def main():
    list = []
    sum = 0

    for i in range(NUM_VALUE):
        num = input('>>>')
        num_int = int(num)
        list.append(num_int)
        sum += num_int

    min_v = min(list)
    max_v = max(list)
    average = sum / NUM_VALUE

    print(min_v, max_v, sum, average)


if __name__ == '__main__':
    main()




