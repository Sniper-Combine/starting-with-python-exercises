
def main():
    try:
        file = open('WorldSeriesWinners.txt', 'r')
        file_winners = file.readlines()

        for i in range(len(file_winners)):
            file_winners[i] = file_winners[i].rstrip('\n')

        team = input('Please, enter team`s name here>>>')

        counter = finding_team(file_winners, team)

        if counter == 0:
            print(f'{team} never won')
        else:
            print(f'{team} was the winner for {counter} years')

    except:
        print('Something went wrong')


def finding_team(list_team, name):
    counter = 0

    for i in list_team:
        if i == name:
            counter += 1

    return counter


if __name__ == '__main__':
    main()
