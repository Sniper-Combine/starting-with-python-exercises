def main():
    right_answers_counter = 0
    mistakes_counter = 0
    counter = 0

    right_list = making_right_answers()
    student_list = student_answers()

    for i in right_list:
        if i == student_list[counter]:
            print(f'{counter + 1}. is correct')
            right_answers_counter += 1
        else:
            print(f'{counter + 1}. is wrong')
            mistakes_counter +=1
        counter += 1

    print(f'\n'
          f'{right_answers_counter} correct answers\n'
          f'{mistakes_counter} wrong answers\n')

    if mistakes_counter > 5:
        print('You didn`t pass the exam')

    print(right_list, student_list)


def making_right_answers():
    file = open('student_solution.txt', 'r')
    list = []
    for i in file:
        i = i.rstrip('\n')
        list.append(i)
    return list


def student_answers():
    student_list = []
    for i in range(1, 21):
        val = (input(f'{i}.>>>'))
        student_list.append(val)
    return student_list



if __name__ == '__main__':
    main()
