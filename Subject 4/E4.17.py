import turtle

turtle.setup(600, 600)

turtle.penup()
turtle.goto(-75, -150)
turtle.pendown()
turtle.left(90)

for i in range(8):
    for j in range(1):
        turtle.forward(300)
        turtle.right(135)

turtle.done()