month = 12
rTotal = 0
rDepth = 0

print('Please, enter the amount of years')
years = int(input())

for i in range(years):
    for j in range(month):
        rDepth = float(input('Please, enter the rainfall depth'))
        rTotal += rDepth

totalMonth = month * years
totalDepth = rTotal / totalMonth
print(f'{totalMonth}, {rTotal}, {totalDepth:.1f}')
