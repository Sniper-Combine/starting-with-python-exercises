total = 1

days = int(input('Enter the amount of days'))

for i in range(1, days):
    total *= 2
    rubles = total // 100
    kopeiki = total % 100
print(f'{rubles:,d} rubles and {kopeiki} kopeek ')