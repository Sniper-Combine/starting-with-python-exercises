total = 1
number = int(input('Please, enter a positive number'))

if number > 0:
    for i in range(1, number + 1):
        total *= i
        print(f'{total}')
else:
    print('Your number is not non-negative')