F = 0

print(f'Celsius \t Fahrenheit \n'
      f'-----------------------')
for C in range(0,21):
    F = 9 * C / 5 + 32
    print(f'{C:<2} {F:>20}')