#variables
bugs = 0
totalBugs = 0

#loop
days = int(input('How many days bug collector works'))
for day in range(days):
    bugs = int(input('How many system errors did bug collector found today? '))
    totalBugs += bugs

#output
print (f'Total value of system errors found for {days} days: {totalBugs} errors')