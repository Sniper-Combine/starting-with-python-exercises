import turtle

setups = 8
lenghFor = 100
angle = 45

turtle.setup(700, 700)
turtle.penup()
turtle.goto(-50, 125)
turtle.pendown()

for i in range(setups):
    for j in range(1):
        turtle.forward(lenghFor)
        turtle.right(angle)
turtle.penup()
turtle.goto(-10, 0)
turtle.write('STOP')
turtle.hideturtle()

turtle.done()