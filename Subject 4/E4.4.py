speed = 90

print('Please enter the amount of hours \n')
hours = int(input())

print(f'Hour\tDistance\n'
      f'----------------\n')

for i in range (1, hours + 1):
    distance = i * speed
    print(f'{i:<5}\t{distance:>7}')