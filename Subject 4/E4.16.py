import turtle

squareValue = 100
widthSq = 10
turtle.screensize(canvwidth=600, canvheight=600, bg=None)
turtle.speed(0)
turtle.penup()
turtle. goto(300, -300)
turtle.pendown()

for i in range(squareValue):
    for j in range(1):
        turtle.setheading(180)
        turtle.forward(widthSq)
        turtle.right(90)
        turtle.forward(widthSq)
        turtle.right(90)
        turtle.forward(widthSq)
        turtle.right(90)
        turtle.forward(widthSq)
        turtle.right(90)
        widthSq += 3



turtle.done()