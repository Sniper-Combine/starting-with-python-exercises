population = 0
raisePerDay = 0
daysForBreed = 0

population = int(input('Please, enter the starting population size'))
raisePerDay = int(input('Please, enter an average daily population increase in %'))
daysForBreed = int(input('Please, enter a number of days for the population to breed'))

print(f'Day\t\tPopulation\n'
      f'--------------------')
if raisePerDay >= 1 and raisePerDay <= 100:
    raisePerDay /= 100
    for i in range(daysForBreed):
        if i > 0:
            population += (population * raisePerDay)
        print(f'{i + 1:<2}\t\t{population}')
else:
    print('% is incorrect')