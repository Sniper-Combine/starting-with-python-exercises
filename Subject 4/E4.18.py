import turtle

lenghInc = 3
coef = 3

turtle.setup(600, 600)
turtle.left(90)
turtle.speed(0)

for i in range(121):
    for j in range(1):
        turtle.forward(lenghInc)
        turtle.left(90)
        lenghInc += coef

turtle.done()