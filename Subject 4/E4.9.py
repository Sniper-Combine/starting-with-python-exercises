oceanLvl = 1.6
totalMM = 0.0

years = int(input('Please, enter a amount of years'))

print(f'Year \t mm \n '
      f'------------')

for i in range(1, years + 1):
    totalMM += oceanLvl
    print(f'{i} \t {totalMM:>5.1f}')
