HALF_CONST = 0.5


def main():
    mass = int(input('Please, enter mass in kg'))
    speed = int(input('Please, enter speed in km/s'))
    print(f'Kinetic energy is {kinetic_energy(mass, speed):.1f}')


def kinetic_energy(mass, speed):
    speed **= 2
    kinet_energy = HALF_CONST * mass * speed
    return kinet_energy


main()