HALF_SOMETHING = 0.5
G = 9.8


def main():
    for i in range(1, 11):
        print(f'{i}\t{falling_distance(i):.1f}')


def falling_distance(value):
    value **= 2
    d = HALF_SOMETHING * G * value
    return d


main()