import turtle



def main():
    turtle.setup(640, 480)
    turtle.speed(0)
    turtle.width(2)
    turtle.hideturtle()

    lower_circle()
    middle_circle()
    head()
    hat()
    face()
    arms()

    turtle.done()

def lower_circle():
    turtle.penup()
    turtle.goto(0, -200)
    turtle.pendown()
    turtle.circle(75)

def middle_circle():
    turtle.penup()
    turtle.goto(0, -50)
    turtle.pendown()
    turtle.circle(45)


def head():
    turtle.penup()
    turtle.goto(0, 40)
    turtle.pendown()
    turtle.circle(25)



def hat():
    turtle.penup()
    turtle.goto(0, 78)
    turtle.pendown()
    turtle.fillcolor('lightblue')
    turtle.begin_fill()
    turtle.begin_fill()
    turtle.forward(30)
    turtle.left(90)
    turtle.forward(15)
    turtle.left(90)
    turtle.forward(15)
    turtle.right(90)
    turtle.forward(30)
    turtle.left(90)
    turtle.forward(30)
    turtle.left(90)
    turtle.forward(30)
    turtle.right(90)
    turtle.forward(15)
    turtle.left(90)
    turtle.forward(15)
    turtle.left(90)
    turtle.forward(30)
    turtle.end_fill()


def face():
    turtle.penup()
    turtle.goto(-8, 65)
    turtle.pendown()
    turtle.circle(4)
    turtle.penup()
    turtle.goto(8, 65)
    turtle.pendown()
    turtle.circle(4)
    turtle.penup()
    turtle.goto(-13, 57)
    turtle.pendown()
    turtle.goto(13, 57)


def arms():
    turtle.penup()
    turtle.goto(-45, 5)
    turtle.pendown()
    turtle.goto(-80, 10)
    turtle.goto(-85, 40)
    turtle.goto(-81, 48)
    turtle.goto(-85, 40)
    turtle.goto(-95, 45)
    turtle.penup()
    turtle.goto(45, 5)
    turtle.pendown()
    turtle.goto(80, 25)
    turtle.goto(90, 23)
    turtle.goto(80, 25)
    turtle.goto(85, 36)


main()