ESTIM_COEF = 0.6
TAX_CONST = 100
TAX_FOR_HUND = 0.72


def main():
    price = int(input('Please, enter the property price'))
    estim_value = estimated_value(price)
    tax_value = tax(estim_value)
    print(f'{estim_value:.2f} and {tax_value:.2f}')


def estimated_value(value):
    estim_value = value * ESTIM_COEF
    return estim_value


def tax(value):
    tax_for_one = value / TAX_CONST
    tax_total = tax_for_one * TAX_FOR_HUND
    return tax_total


main()