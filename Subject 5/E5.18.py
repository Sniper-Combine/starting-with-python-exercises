def main():
    for i in range(1, 101):
        if is_prime(i):
            print(f'{i}', end=' ')


def is_prime(value):
    num = int(value / 2)
    status = True

    for i in range(2, num + 1):
        if value % i == 0:
            status = False

    return status



main()