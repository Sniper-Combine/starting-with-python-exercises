SQUARE_CONST = 10
PAINT_CONST = 5
HOURS_CONST = 8
PAYMENT_CONST = 2000


def main():
    print('Enter the surface square')
    surf = int(input())
    print('Enter the paint cost')
    paint_cost = int(input())

    paint_needed(surf)
    print(f'Paint cans needed: {paint_needed(surf):.1f}')

    working_hours(surf)

    paint_const_calc = paint_price(paint_needed(surf), paint_cost)
    print(f'Paint will cost you {paint_const_calc:.1f}Rub')

    work_cost_amount = int(work_cost(surf))
    print(f'Whole work will cost you {work_cost_amount:.1f}Rub')

    total_cost(work_cost_amount, paint_const_calc)


def paint_needed(value):
    paint_need = (value / SQUARE_CONST)
    return paint_need


def working_hours(value):
    hours_need = value / SQUARE_CONST * HOURS_CONST
    print(f'Work will last for {hours_need} hours')
    return hours_need


def paint_price(value1, value2):
    paint_cost_amount = value1 * value2
    return paint_cost_amount


def work_cost(value):
    work_cost_amount = value / SQUARE_CONST * PAYMENT_CONST
    return work_cost_amount


def total_cost(value1, value2):
    total_cost_amount = value1 + value2
    print(f'The entire cost will be {total_cost_amount:.1f}Rub')


main()
