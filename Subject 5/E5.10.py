INCHES_CONST = 12


def main():
    print('Please, enter the amount of feet')
    feet = int(input())
    inches = feet_to_inches(feet)
    print(f'There are {inches} in {feet} feet')


def feet_to_inches(value):
    inches = value * INCHES_CONST
    return inches


main()