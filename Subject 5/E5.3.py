perc = 0.8


def main():
    price = int(input('Please, enter the housing cost'))
    calculating_the_insurance(price)


def calculating_the_insurance(value):
    min_insurance = value * perc
    print(f'You suppose to insure housing at least for {min_insurance:.1f}')


main()
