import random


def main():
    counter = 1
    number = int(input('Please, enter your number'))
    randomized_number = randomizer()
    while number != randomized_number:
        counter += 1
        if number > randomized_number:
            print(f'Your number is bigger than random {randomized_number}')
            number = int(input('Please, enter your number'))
        else:
            print(f'Your number is lesser than random {randomized_number}')
            number = int(input('Please, enter your number'))
    else:
        print(f'Congratulation! You guessed right! You tried to guess for {counter} times')


def randomizer():
    rand_num = random.randint(1, 100)
    return rand_num


main()