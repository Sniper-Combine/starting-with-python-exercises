def main():
    val1 = int(input('Please, enter the 1-st value'))
    val2 = int(input('Please, enter the 2-nd value'))
    val3 = int(input('Please, enter the 3-d value'))
    val4 = int(input('Please, enter the 4-th value'))
    val5 = int(input('Please, enter the 5-th value'))

    average = calc_average(val1, val2, val3, val4, val5)
    print(f'{average:.1f}')
    determine_grade(average)

def calc_average(val1, val2, val3, val4, val5):
    average = (val1 + val2 + val3 + val4 + val5) / 5
    return average


def determine_grade(value):
    if value >= 90:
        print('A')
    elif value >= 80 and value <= 89:
        print('B')
    elif value >= 70 and value <= 79:
        print('C')
    elif value >= 60 and value <= 69:
        print('D')
    else:
        print('F')


main()