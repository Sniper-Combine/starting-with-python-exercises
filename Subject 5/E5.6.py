CAL_OF_FATS = 9
CAL_OF_CAR = 4


def main():
    fats()
    carbohydrates()
    

def fats():
    print('Enter the amount of fat grams')
    fats_got = int(input())
    fat_cal_got = fats_got * CAL_OF_FATS
    print(f'Calories, gotten from fats: {fat_cal_got}')


def carbohydrates():
    print('Enter the amount of carbohydrates grams')
    carb_got = int(input())
    carb_cal_got = carb_got * CAL_OF_CAR
    print(f'Calories, gotten from carbohydrates: {carb_cal_got}')


main()