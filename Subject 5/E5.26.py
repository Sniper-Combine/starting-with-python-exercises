import turtle
import random


def main():
    turtle.setup(600, 600)
    turtle.bgcolor('black')
    turtle.speed(0)
    turtle.hideturtle()

    stars()
    city()
    windows()

    turtle.done()


def city():
    col = '#696969'
    turtle.speed(0)
    turtle.penup()
    turtle.goto(-300, -100)
    turtle.pendown()
    turtle.fillcolor(col)
    turtle.begin_fill()

    turtle.forward(65)
    turtle.left(90)

    turtle.forward(150)
    turtle.right(90)

    turtle.forward(80)
    turtle.left(90)

    turtle.forward(180)
    turtle.right(90)

    turtle.forward(120)
    turtle.right(90)

    turtle.forward(250)
    turtle.left(90)

    turtle.forward(80)
    turtle.left(90)

    turtle.forward(120)
    turtle.right(90)

    turtle.forward(100)
    turtle.right(90)

    turtle.forward(100)
    turtle.left(90)

    turtle.forward(60)
    turtle.right(90)


    turtle.forward(120)
    turtle.left(90)

    turtle.forward(100)
    turtle.right(90)

    turtle.forward(420)
    turtle.right(90)

    turtle.forward(600)

    turtle.end_fill()


def stars():
    for j in range(60):
        x = random.randint(-300, 300)
        y = random.randint(-100, 300)
        for i in range(1):
            turtle.penup()
            turtle.goto(x, y)
            turtle.pendown()
            turtle.dot(3, 'white')


def windows():
    turtle.penup()
    turtle.goto(-120, 180)
    turtle.pendown()
    turtle.fillcolor('white')
    turtle. begin_fill()
    for i in range(4):
        turtle.forward(20)
        turtle.right(90)
    turtle.end_fill()

    turtle.penup()
    turtle.goto(-120, 150)
    turtle.pendown()
    turtle.fillcolor('white')
    turtle.begin_fill()
    for i in range(4):
        turtle.forward(20)
        turtle.right(90)
    turtle.end_fill()

    turtle.penup()
    turtle.goto(-60, 110)
    turtle.pendown()
    turtle.fillcolor('white')
    turtle.begin_fill()
    for i in range(4):
        turtle.forward(20)
        turtle.right(90)
    turtle.end_fill()

    turtle.penup()
    turtle.goto(-200, 0)
    turtle.pendown()
    turtle.fillcolor('white')
    turtle.begin_fill()
    for i in range(4):
        turtle.forward(20)
        turtle.right(90)
    turtle.end_fill()

    turtle.penup()
    turtle.goto(0, -60)
    turtle.pendown()
    turtle.fillcolor('white')
    turtle.begin_fill()
    for i in range(4):
        turtle.forward(20)
        turtle.right(90)
    turtle.end_fill()

    turtle.penup()
    turtle.goto(130, 70)
    turtle.pendown()
    turtle.fillcolor('white')
    turtle.begin_fill()
    for i in range(4):
        turtle.forward(20)
        turtle.right(90)
    turtle.end_fill()



main()