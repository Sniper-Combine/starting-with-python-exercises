import turtle

def main():

    turtle.setup(700, 700)
    turtle.speed(0)

    square()

    turtle.done()


def square():
    turtle.width(2)
    turtle.hideturtle()
    turtle.penup()
    turtle.goto(-125, 125)
    turtle.pendown()

    for r in range(2):
        for i in range(1, 6):
            if i % 2 != 0:
                turtle.fillcolor('black')
                turtle.begin_fill()
                for j in range(4):
                    turtle.forward(50)
                    turtle.right(90)
                turtle.end_fill()
                turtle.forward(50)
            else:
                for j in range(4):
                    turtle.forward(50)
                    turtle.right(90)
                turtle.forward(50)

        turtle.right(90)
        turtle.forward(100)
        turtle.right(90)

        for i in range(1, 6):
            if i % 2 == 0:
                turtle.fillcolor('black')
                turtle.begin_fill()
                for j in range(4):
                    turtle.forward(50)
                    turtle.right(90)
                turtle.end_fill()
                turtle.forward(50)
            else:
                for j in range(4):
                    turtle.forward(50)
                    turtle.right(90)
                turtle.forward(50)

        turtle.right(180)

    for i in range(1, 6):
        if i % 2 != 0:
            turtle.fillcolor('black')
            turtle.begin_fill()
            for j in range(4):
                turtle.forward(50)
                turtle.right(90)
            turtle.end_fill()
            turtle.forward(50)
        else:
            for j in range(4):
                turtle.forward(50)
                turtle.right(90)
            turtle.forward(50)



main()