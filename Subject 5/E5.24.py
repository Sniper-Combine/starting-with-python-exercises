import turtle


def main():
    turtle.setup(600, 600)
    turtle.speed(0)
    turtle.width(2)
    turtle.hideturtle()

    outer_square()
    middle_square()
    inner_square()
    lines()

    turtle.done()

def outer_square():
    turtle.penup()
    turtle.goto(-150, 150)
    turtle.pendown()
    for i in range(4):
        turtle.forward(300)
        turtle.right(90)



def middle_square():
    turtle.penup()
    turtle.goto(-120, 120)
    turtle.pendown()
    for i in range(4):
        turtle.forward(240)
        turtle.right(90)


def inner_square():
    turtle.penup()
    turtle.goto(-90, 90)
    turtle.pendown()
    turtle.fillcolor('black')
    turtle.begin_fill()
    for i in range(4):
        turtle.forward(180)
        turtle.right(90)
    turtle.end_fill()


def lines():
    turtle.penup()
    turtle.goto(-150, 150)
    turtle.pendown()
    turtle.goto(-90, 90)

    turtle.penup()
    turtle.goto(150, 150)
    turtle.pendown()
    turtle.goto(90, 90)

    turtle.penup()
    turtle.goto(-150, -150)
    turtle.pendown()
    turtle.goto(-90, -90)

    turtle.penup()
    turtle.goto(150, -150)
    turtle.pendown()
    turtle.goto(90, -90)

    turtle.penup()
    turtle.goto(150, 0)
    turtle.pendown()
    turtle.goto(90, 0)

    turtle.penup()
    turtle.goto(-150, 0)
    turtle.pendown()
    turtle.goto(-90, 0)

    turtle.penup()
    turtle.goto(0, -150)
    turtle.pendown()
    turtle.goto(0, -90)

    turtle.penup()
    turtle.goto(0, 150)
    turtle.pendown()
    turtle.goto(0, 90)

main()