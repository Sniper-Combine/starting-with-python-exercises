MUNICIPAL_TAX = 0.025
FEDERAL_TAX = 0.05


def main():
    print('Please, enter the total sales amount $')
    sales = int(input())

    fed_tax = federal_tax(sales)
    mun_tax = municipal_tax(sales)
    total_taxes = total_tax(mun_tax, fed_tax)

    print(f'Total tax: {total_taxes:.1f}$ \n'
          f'Federal tax: {fed_tax:.1f}$ \n'
          f'Municipal tax: {mun_tax:.1f}$')


def federal_tax(value):
    fed_tax = value * FEDERAL_TAX
    return fed_tax


def municipal_tax(value):
    mun_tax = value * MUNICIPAL_TAX
    return mun_tax


def total_tax(value1, value2):
    total_tax = value1 + value2
    return total_tax


main()
