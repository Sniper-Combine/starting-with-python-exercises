def main():
    km = int(input('Please, Enter the amount of kilometres'))
    coef = 0.6214
    miles = km * coef
    return miles

mainForPrint = main()

print (f'{mainForPrint:.2f}')