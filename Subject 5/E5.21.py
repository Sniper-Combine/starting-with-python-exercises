from random import randint

ROCK = 1
PAPER = 2
SCISSORS = 3


def main():
    comp_choice = comp_plays()
    player_choice = int(input('Your choice!'))
    if player_choice != 1 and player_choice != 2 and player_choice != 3:
        print('Your choice is incorrect')
    else:
        if player_choice == 1 and comp_choice == 1:
            print('Draw', comp_choice)
        elif player_choice == 1 and comp_choice == 2:
            print('You won', comp_choice)
        elif player_choice == 1 and comp_choice == 3:
            print('You loose', comp_choice)
        elif player_choice == 2 and comp_choice == 1:
            print('You won', comp_choice)
        elif player_choice == 2 and comp_choice == 2:
            print('You loose', comp_choice)
        elif player_choice == 2 and comp_choice == 3:
            print('Draw', comp_choice)
        elif player_choice == 3 and comp_choice == 1:
            print('You loose', comp_choice)
        elif player_choice == 3 and comp_choice == 2:
            print('Draw', comp_choice)
        elif player_choice == 3 and comp_choice == 3:
            print('You won', comp_choice)


def comp_plays():
    comp_choice = randint(1, 3)
    return comp_choice

main()