stateTax = 0.025
fedTax = 0.05


def main():
    price = int(input('Please, enter the housing cost'))
    calculation(price)


def calculation(value):
    total_tax = value * (stateTax + fedTax)
    total_state_tax = value * stateTax
    total_fed_tax = value * fedTax
    total = value + total_tax
    print(f'{total:.1f}, {total_tax:.1f}, {total_state_tax:.1f}, {total_fed_tax:.1f}')


main()
