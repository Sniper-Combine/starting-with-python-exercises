import random


def main():
    sum_ret = sum()
    print('Enter your answer')
    answer = int(input())

    if sum_ret == answer:
        print('Congratulations. Your answer is correct')
    else:
        print('Shame. Your answer is incorrect')


def sum():
    value1 = random.randint(1, 1001)
    value2 = random.randint(1, 1001)
    print(f'  {value1} \n'
          f'+ {value2}')
    sum = value1 + value2
    return sum


main()