classA_cost = 20
classB_cost = 15
classC_cost = 10


def main():
    classA_sold = classA()
    classB_sold = classB()
    classC_sold = classC()
    total_profit = (classA_sold + classB_sold + classC_sold)
    print(total_profit)


def classA():
    print('How many A class tickets have been sold')
    classA_sold = int(input())
    classA_profit = classA_sold * classA_cost
    return classA_profit


def classB():
    print('How many B class tickets have been sold')
    classB_sold = int(input())
    classB_profit = classB_sold * classB_cost
    return classB_profit


def classC():
    print('How many C class tickets have been sold')
    classC_sold = int(input())
    classC_profit = classC_sold * classC_cost
    return classC_profit


main()