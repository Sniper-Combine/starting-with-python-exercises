BMI = 0
strPrint = ''


mass = int(input('Please, enter your mass in kg'))
height = float(input('Please, enter your height in meters'))

BMI = mass / height ** 2

if mass <= 20 and mass >= 250:
    print('Your mass is incorrect')
elif height <= 1.0 and height >= 2.5:
    print('Your height is incorrect')
else:
    if BMI < 18.5:
        strPrint = 'Your BMI is too low. Your are in underweight range'
    if BMI >  25:
        strPrint = 'Your BMI is too heigh. Your are in overweight range'
    else:
        strPrint = 'Your BMI is OK. You are within healthy weight range'
    print(f'Your BMI is {BMI:.1f} \n'
          f'{strPrint}')
