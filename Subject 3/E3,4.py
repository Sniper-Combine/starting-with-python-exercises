#Entering number
num = int(input('Enter a positive integer in range from 1 to 10'))

#Creating logic
if num == 1:
    print('An equal Roman number is I')
elif num == 2:
    print('An equal Roman number is II')
elif num == 3:
    print('An equal Roman number is III')
elif num == 4:
    print('An equal Roman number is IV')
elif num == 5:
    print('An equal Roman number is V')
elif num == 6:
    print('An equal Roman number is VI')
elif num == 7:
    print('An equal Roman number is VII')
elif num == 8:
    print('An equal Roman number is VIII')
elif num == 9:
    print('An equal Roman number is IX')
elif num == 10:
    print('An equal Roman number is X')
else:
    print ('Current number is incorrect')
