pocket = 0

pocket = int(input('Please, enter a pocket number from 0 to 36'))
evenNum = pocket % 2
if pocket > 36 or pocket < 0:
    print('Current number is incorrect')
else:
    if pocket > 0 and pocket <= 10:
        if evenNum == 0:
            print('black')
        else:
            print('red')
    elif pocket >= 11 and pocket <= 18:
        if evenNum == 0:
            print('red')
        else:
            print('black')
    elif pocket >= 19 and pocket <= 28:
        if evenNum == 0:
            print('black')
        else:
            print('red')
    elif pocket >= 29 and pocket <= 36:
        if evenNum == 0:
            print('red')
        else:
            print('black')
    else:
        print('green')
