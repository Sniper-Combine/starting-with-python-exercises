sec = int(input('Enter number of second'))

if sec >= 86400:
    days = sec // 86400
    hoursMinDays = sec % 86400

if sec >= 3600:
    hours = sec // 3600
    minutesMinHours = sec % 3600

if sec >= 60:
    min = sec // 60
    secMinMinutes = sec % 60

if min == 0:
    print('Current amount of seconds is less than one minute')
else:
    print(f'{sec} equals:'
          f'{min} minutes and {secMinMinutes} seconds')
    if hours != 0:
        print(f'{hours} hours and {minutesMinHours} seconds')
    if days != 0:
        print(f'{days} days and {hoursMinDays} seconds')