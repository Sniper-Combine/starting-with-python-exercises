## Entering global variables
ruble = 100
strPrint = ''
#Denomination of coins
five_kopeek = 5
ten_kopeek = 10
fifty_kopeek = 50
#Amount of inserted coins
amountOfFive = 0
amountOfTen = 0
amountOfFifty = 0
#The product of the amount of coins to their denomination
prodOffive = 0
prodOfTen = 0
prodOfFifty = 0
#Summ of all coins
sumKopeek = 0

## Inserting coins
amountOfFive = int(input('How many coins of 5 are you supposed to insert'))
amountOfTen = int(input('How many coins of 10 are you supposed to insert'))
amountOfFifty = int(input('How many coins of 50 are you supposed to insert'))

#Calculating product
prodOffive = amountOfFive * five_kopeek
prodOfTen = amountOfTen * ten_kopeek
prodOfFifty = amountOfFifty * fifty_kopeek
sumKopeek = prodOffive + prodOfTen + prodOfFifty

## Creating Logic
if sumKopeek == ruble:
    strPrint = 'Well done. The amount of inserted coins equals to a ruble'
elif sumKopeek < ruble:
    strPrint = 'Not enough. The amount of inserted coins is less than ruble'
else:
    strPrint = 'Too much. The amount of inserted coins is more than ruble'

print(strPrint)

