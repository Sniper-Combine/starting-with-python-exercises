#Creating global variables
r = ('red')
y = ('yellow')
b = ('blue')

#Entering color`s names
first_color = str(input('Please, write the first color in lowercase'))
second_color = str(input('Please, write the second color in lowercase'))

#Checking the accessibility of both colors
if first_color != r and first_color != y and first_color != b:
    print('First color is incorrect')
elif second_color != r and second_color != y and second_color != b:
    print('Second color is incorrect')
elif first_color == second_color:
    print('Both colors are equal')
#both colors are correct, so we can mix them together now
else:
    if first_color == r:
        if second_color == b:
            print('violet')
        else:
            print('orange')
    elif first_color == b:
        if second_color == r:
            print('violet')
        else:
            print('green')
    else:
        if second_color == r:
            print('orange')
        else:
            print('green')