#Entering a valid age
age = float(input('Please, enter an age in full years'))

#Creating logic
if age <= 1:
    print('Baby')
elif age < 13:
    print('Child')
elif age < 20:
    print('Teenager')
else:
    print('Adult')
