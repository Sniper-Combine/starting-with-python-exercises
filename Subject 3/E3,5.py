#Creating constant
coef = float(9.8)

#Creating variables
mass = float(input('Please, enter a mass value'))
weight = mass * coef

#Creating logic
if weight > 500:
    print('Current weight is too heavy')
elif weight < 100:
    print('Current weight is too light')
else:
    print(f'Current weight is {weight:.1f}N')
