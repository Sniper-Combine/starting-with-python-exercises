#Вводим длину и ширину двух прямоугольников
first_length = float(input("Please, enter a first rectangle`s length"))
first_width = float(input("Please, enter a first rectangle`s width"))

second_length = float(input("Please, enter a second rectangle`s length"))
second_width = float(input("Please, enter a second rectangle`s width"))

#Вычисляем площадь двух прямоугольников
Square_first = first_length * first_width
Square_second = second_length * second_width

#Создаем логику(вычисляем, площадь какого из прямоугольников больше)
if first_length <= 0 or first_width <= 0 or second_length <= 0 or second_width <= 0:
    print('Одно из введенных значений - некорректно. Пожалуйста, введите верное число')
else:
    if Square_first > Square_second:
        print(f"The first rectangle`s square ({Square_first}) is larger, than the second one`s ({Square_second})")
    elif Square_first < Square_second:
        print(f"The second rectangle`s square ({Square_second}) is larger, than the first one`s ({Square_first})")
    else:
        print(f"The squares of both rectangle`s ({Square_first}) are equal")
