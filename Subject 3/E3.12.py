price = 99
discount = 0
totalDiscount = 0

goods = int(input('How many packages did you buy'))

total = goods * price
if goods < 0:
    print('Current value is incorrect')
elif goods > 0 and goods < 10:
    discount = 0
elif goods >= 10 and goods <=19:
    discount = 0.1
elif goods >= 20 and goods <= 49:
    discount = 0.2
elif goods >= 50 and goods <= 99:
    discount = 0.3
else:
    discount = 0.4
if discount == 0:
    print(f'You have no discount \n'
          f'Final price is {total}$')
else:
    totalDiscount = total * discount
    total = total - totalDiscount
    print(f'Your discount is {totalDiscount}$ \n'
          f'Final price is {total}$')
