leapYear = True
year = 0

year = int(input('Enter a year'))

if year % 100 == 0:
    if year % 400 == 0:
        leap_year = True
    else:
        leap_year = False
else:
    if year % 4 == 0:
        leap_year = True
    else:
        leap_year = False
if leap_year:
    print(f'{year} is a leap year')
else:
    print(f'{year} isn`t a leap year')
