rate = 0
rateforhundred = 100
priceForOneGram = 0
price = 0
strPrint = ''

weight = int(input('Please, enter package weight'))
if weight <= 0:
    print('Current weight value is incorrect')
elif weight <= 200:
    rate = 150
elif weight < 600:
    rate = 300
elif weight < 1000:
    rate = 400
else:
    rate = 475
priceForOneGram = rate / rateforhundred
price = priceForOneGram * weight

print(f'Your rate: {rate} \n'
      f'Your price: {price:.1f}')
